import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Section } from '../section/section';
import { ActionModalComponent } from '../modal/action-modal/action-modal.component';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  buttons: any;

  @Output('addSection') addSection: EventEmitter<Section> = new EventEmitter();

  @Output('addSubSection') addSubSection: EventEmitter<
    any
  > = new EventEmitter();

  @Output('deleteSection') deleteSection: EventEmitter<
    Section
  > = new EventEmitter();
  @Output('deleteSubSection') deleteSubSection: EventEmitter<
    any
  > = new EventEmitter();

  constructor(public dialog: MatDialog) {}

  ngOnInit(): void {}

  addActionItems(): void {
    const dialogRef = this.dialog.open(ActionModalComponent, {
      width: '250px',
      data: { save: this.buttons },
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.buttons = result;
    });
  }
}
