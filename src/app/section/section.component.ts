import { Component, OnInit, Input, Output } from '@angular/core';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Section } from './section';
import { SharedServiceService } from '../shared/shared-service.service';

@Component({
  selector: 'app-section',
  templateUrl: './section.component.html',
  styleUrls: ['./section.component.scss'],
})
export class SectionComponent implements OnInit {
  @Input() sections: Section[];

  selectedSection: Section;

  selectedIndex: number;
  selectedIndex2: number;

  constructor(private sharedService: SharedServiceService) {}

  ngOnInit(): void {
    this.sharedService.getActiveSection().subscribe((response) => {
      this.selectedSection = response;
      this.selectedIndex = this.sections.findIndex(obj => obj.id === this.selectedSection.id);
      console.log('Active section:: ', this.selectedSection);
    });
  }

  drop(event: CdkDragDrop<string[]>, arrayMove: any): void {
    moveItemInArray(arrayMove, event.previousIndex, event.currentIndex);
  }

  // public setRow(index: number): void {
  //   console.log('id:::: ', this.sections[index].id === this.section.id);
  //   this.selectedIndex = index;

  //   const ind = this.sections.findIndex(obj => obj.id === this.section.id);
  //   console.log(ind);
  // }

  public setRows(index: number): void {
    this.selectedIndex2 = index;
  }
}
