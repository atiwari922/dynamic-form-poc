import { Component, OnInit } from '@angular/core';
import { Section } from './section/section';
import { SharedServiceService } from './shared/shared-service.service';
import * as uuid from 'uuid';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'Angular-POC';

  constructor(private sharedService: SharedServiceService) {}

  sections = [
    {
      id: 'Section1',
      title: 'Section 1',
      formFields: [{ title: 'Label' }, { title: 'Label' }],
      subSections: [
        {
          id: 'SubSection1',
          title: 'Sub-Section 1',
          formFields: [{ title: 'Label' }, { title: 'Label' }],
        },
        {
          id: 'SubSection2',
          title: 'Sub-Section 2',
          formFields: [{ title: 'Label' }, { title: 'Label' }],
        },
      ],
    },
    {
      id: 'Section2',
      title: 'Section 2',
      formFields: [{ title: 'Label' }, { title: 'Label' }],
      subSections: [
        {
          id: 'SubSection1',
          title: 'Sub-Section 1',
          formFields: [{ title: 'Label' }, { title: 'Label' }],
        },
      ],
    },
  ];

  selectedSection: Section = this.sections[0];

  ngOnInit(): void {
    this.activeSection();
  }

  selectSection(data): void {
    this.selectedSection = data;
  }

  addSection(): void {
    const myId = uuid.v4();
    const sectionId = `Section${myId}`;
    const sectionName = `Section ${this.sections.length + 1}`;
    const newSection = {
      id: sectionId,
      title: sectionName,
      formFields: [{ title: 'Label' }, { title: 'Label' }],
      subSections: [
        {
          id: 'SubSection1',
          title: 'Sub-Section 1',
          formFields: [{ title: 'Label' }, { title: 'Label' }],
        },
      ],
    };
    this.sections.push(newSection);
    this.selectedSection = newSection;
  }

  deleteSection(): void {
    // if (this.sections.length <= 1) {
    //   this.selectedSection = this.sections[0];
    // } else {
    //   this.sections.splice(-1, 1);
    //   this.selectedSection = this.sections[0];
    // }
    for (let i = 0; i < this.sections.length; i++) {
      if (this.sections[i].id === this.selectedSection.id) {
        this.sections.splice(i, 1);
        this.selectedSection = this.sections[0];
      }
    }
  }

  deleteSubSection(): void {
    this.selectedSection.subSections.splice(-1, 1);
  }

  addSubSection(): void {
    const subSectionId = `Sub-Section${
      this.selectedSection.subSections.length + 1
    }`;
    const subSectionName = `Sub-Section ${
      this.selectedSection.subSections.length + 1
    }`;
    const newSubsection = {
      id: subSectionId,
      title: subSectionName,
      formFields: [{ title: 'Label' }],
    };
    this.selectedSection.subSections.push(newSubsection);
  }

  activeSection(): void {
    this.sharedService.setActiveSection(this.selectedSection);
  }
}
