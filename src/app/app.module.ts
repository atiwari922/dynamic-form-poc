import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { MainContentComponent } from './main-content/main-content.component';
import { SectionComponent } from './section/section.component';
import { SubSectionComponent } from './sub-section/sub-section.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { ActionModalComponent } from './modal/action-modal/action-modal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedServiceService } from './shared/shared-service.service';
import { MaterialModule } from './material/material.module';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidebarComponent,
    MainContentComponent,
    SectionComponent,
    SubSectionComponent,
    ActionModalComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    DragDropModule,
    ReactiveFormsModule,
    MaterialModule,
    FormsModule,
  ],
  providers: [SharedServiceService],
  bootstrap: [AppComponent],
  entryComponents: [ActionModalComponent],
})
export class AppModule {}
