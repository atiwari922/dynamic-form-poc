import { Injectable } from '@angular/core';
import { Section } from '../section/section';
import { BehaviorSubject, Observable } from 'rxjs';
import { SectionComponent } from '../section/section.component';

@Injectable({
  providedIn: 'root',
})
export class SharedServiceService {
  public activeSection$: BehaviorSubject<Section> = new BehaviorSubject(null);

  constructor() {}

  setActiveSection(section: Section): void {
    this.activeSection$.next(section);
  }

  getActiveSection(): Observable<Section> {
    return this.activeSection$.asObservable();
  }
}
